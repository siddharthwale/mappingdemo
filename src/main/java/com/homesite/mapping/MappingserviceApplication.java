package com.homesite.mapping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MappingserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MappingserviceApplication.class, args);
	}

}
