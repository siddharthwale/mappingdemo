package com.homesite.mapping.service;

import com.homesite.mapping.model.Field;
import com.homesite.mapping.model.Mapping;
import com.homesite.mapping.model.MappingRequest;
import com.homesite.mapping.model.MappingResponse;
import com.homesite.mapping.repository.MappingRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MappingService {

    private MappingRepository mappingRepository;

    @Autowired
    public MappingService(MappingRepository mappingRepository) {
        this.mappingRepository = mappingRepository;
    }

    MappingResponse mappingResponse = new MappingResponse();
    private List<Field> convertedFields = new ArrayList<>();

    public MappingResponse convertMapping(MappingRequest mappingRequest) {
        //Getting the mapping list from the database
        List<Mapping> mappingList = mappingRepository.findAllByStateCodeAndUwCompanyAndPreviousUwCompany(mappingRequest.getStateCode(), mappingRequest.getUwCompany(), mappingRequest.getPreviousUwCompany());
        mappingResponse.setForm_code(mappingRequest.getFormCode());
        mappingResponse.setState_code(mappingRequest.getStateCode());
        mappingResponse.setUw_company(mappingRequest.getUwCompany());

        if (mappingList != null && mappingList.size() > 0) {
            List<Field> fields = mappingRequest.getFields();
            if (fields != null && fields.size() > 0) {
                //Iterating through the list of fields from the request
                for (Field f : fields) {
                    if (mappingList.stream().anyMatch(mapping -> mapping.getKeyName().equals(f.getField_name()))) {
                        //Getting the list of mappings for a particular field name
                        List<Mapping> distinctMappings = mappingList.stream().filter(mapping -> mapping.getKeyName().equals(f.getField_name())).collect(Collectors.toList());
                        //if distinct mapping == 1 means it is a direct mapping
                        if (distinctMappings.size() == 1) {
                            mappingResponse = directMapping(distinctMappings, f);
                        } else {
                            //if distinct mapping > 1 means it has more than one mapping
                            //Checking if the formula column is null and we have conversion filter for a particular mapping
                            if (distinctMappings.stream().allMatch(mapping -> mapping.getFormula() == null)) {
                                mappingResponse = multipleMappingWithConversionFilter(distinctMappings, fields, f);
                            } else {
                                mappingResponse = multipleMappingWithConversionFilterAndFormula(distinctMappings, fields, f);
                            }
                        }
                    }
                }
            }
        }
        return mappingResponse;
    }

    private MappingResponse directMapping(List<Mapping> distinctMappings, Field f) {
        Mapping mapping = distinctMappings.get(0);
        if (mapping.getConversionFilter() == null && mapping.getFormula() == null) {
            if (mapping.getKeyValue().equals(f.getField_value())) {
                Field convertedField = new Field();
                convertedField.setField_name(f.getField_name());
                convertedField.setField_value(mapping.getConvertedKeyValue());
                convertedFields.add(convertedField);
                mappingResponse.setFields(convertedFields);
            }
        }
        return mappingResponse;
    }

    private MappingResponse multipleMappingWithConversionFilter(List<Mapping> distinctMappings, List<Field> fields, Field f) {
        for (Mapping mapping : distinctMappings) {
            //Converting conversion filter in string to json object
            JSONObject jsonObject = new JSONObject(mapping.getConversionFilter());
            //Detremining JSON Object length
            int jsonLen = jsonObject.length();
            int count = 0;
            //Iterating through the json object
            for (Object key : jsonObject.keySet()) {
                String keyStr = (String) key;
                Field field = fields.stream().filter(field1 -> field1.getField_name().equalsIgnoreCase(keyStr)).findAny().get();
                Object keyvalue = jsonObject.get(keyStr);
                if (field.getField_value().equals(keyvalue.toString())) {
                    count++;
                }
                // if count ==jsonlen which means we have iterated through all the json object
                // and got the mapping which one to use after comapring the key values
                if (count == jsonLen) {
                    Field convertedField = new Field();
                    convertedField.setField_name(f.getField_name());
                    convertedField.setField_value(mapping.getConvertedKeyValue());
                    convertedFields.add(convertedField);
                    mappingResponse.setFields(convertedFields);
                }
            }
        }
        return mappingResponse;
    }

    private MappingResponse multipleMappingWithConversionFilterAndFormula(List<Mapping> distinctMappings, List<Field> fields, Field f) {
        List<String> fieldStream = fields.stream().map(field -> field.getField_name()).collect(Collectors.toList());
        for (Mapping mapping : distinctMappings) {
            //Converting conversion filter in string to json object
            JSONObject jsonObject = new JSONObject(mapping.getConversionFilter());
            //Detremining JSON Object length
            int jsonLen = jsonObject.length();
            int count = 0;
            String formula = null;
            //Iterating through the json object
            for (Object key : jsonObject.keySet()) {
                String keyStr = (String) key;
                Field field = fields.stream().filter(field1 -> field1.getField_name().equalsIgnoreCase(keyStr)).findAny().get();
                Object keyvalue = jsonObject.get(keyStr);
                if (field.getField_value().equals(keyvalue.toString())) {
                    count++;
                }
                // if count ==jsonlen which means we have iterated through all the json object
                // and got the mapping which one to use after comapring the key values
                if (count == jsonLen) {
                    formula = mapping.getFormula();
                    for (String s : fieldStream) {
                        if (mapping.getFormula().contains(s)) {
                            Field formulaField = fields.stream().filter(field1 -> field1.getField_name().equalsIgnoreCase(s)).findAny().get();
                            formula = formula.replace(s, formulaField.getField_value());
                        }
                    }

                }
                if (formula != null) {
                    ScriptEngineManager mgr = new ScriptEngineManager();
                    ScriptEngine engine = mgr.getEngineByName("JavaScript");
                    Field convertedField = new Field();
                    convertedField.setField_name(f.getField_name());
                    try {
                        convertedField.setField_value(engine.eval(formula).toString());
                    } catch (ScriptException e) {
                        e.printStackTrace();
                    }
                    convertedFields.add(convertedField);
                    mappingResponse.setFields(convertedFields);
                }

            }
        }
        return mappingResponse;
    }
}
