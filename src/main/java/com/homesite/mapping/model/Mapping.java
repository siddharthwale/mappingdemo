package com.homesite.mapping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "hs_mapping")
public class Mapping {
    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "uw_company")
    private int uwCompany;

    @Column(name = "previous_uw_company")
    private int previousUwCompany;

    @Column(name = "state_code")
    private String stateCode;

    @Column(name = "key_name")
    private String keyName;

    @Column(name = "field_value")
    private String keyValue;

    @Column(name = "converted_field_value")
    private String convertedKeyValue;

    @Column(name = "conversion_filter")
    private String conversionFilter;

    @Column(name = "formula")
    private String formula;

    public Mapping() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getUwCompany() {
        return uwCompany;
    }

    public void setUwCompany(int uwCompany) {
        this.uwCompany = uwCompany;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public String getConvertedKeyValue() {
        return convertedKeyValue;
    }

    public void setConvertedKeyValue(String convertedKeyValue) {
        this.convertedKeyValue = convertedKeyValue;
    }

    public String getConversionFilter() {
        return conversionFilter;
    }

    public void setConversionFilter(String conversionFilter) {
        this.conversionFilter = conversionFilter;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public int getPreviousUwCompany() {
        return previousUwCompany;
    }

    public void setPreviousUwCompany(int previousUwCompany) {
        this.previousUwCompany = previousUwCompany;
    }

    @Override
    public String toString() {
        return "Mapping{" +
                "id=" + id +
                ", uwCompany=" + uwCompany +
                ", previousUwCompany=" + previousUwCompany +
                ", stateCode='" + stateCode + '\'' +
                ", keyName='" + keyName + '\'' +
                ", keyValue='" + keyValue + '\'' +
                ", convertedKeyValue='" + convertedKeyValue + '\'' +
                ", conversionFilter='" + conversionFilter + '\'' +
                ", formula='" + formula + '\'' +
                '}';
    }
}
