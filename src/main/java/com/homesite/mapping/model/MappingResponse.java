package com.homesite.mapping.model;

import java.util.List;

public class MappingResponse {
    private int uw_company;
    private String state_code;
    private String form_code;
    private List<Field> fields;

    public MappingResponse(int uw_company, String state_code, String form_code, List<Field> fields) {
        this.uw_company = uw_company;
        this.state_code = state_code;
        this.form_code = form_code;
        this.fields = fields;
    }

    public MappingResponse() {
    }

    public int getUw_company() {
        return uw_company;
    }

    public void setUw_company(int uw_company) {
        this.uw_company = uw_company;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getForm_code() {
        return form_code;
    }

    public void setForm_code(String form_code) {
        this.form_code = form_code;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return "MappingRequest{" +
                "uw_company='" + uw_company + '\'' +
                ", state_code='" + state_code + '\'' +
                ", form_code='" + form_code + '\'' +
                ", fields=" + fields +
                '}';
    }
}
