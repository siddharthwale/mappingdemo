package com.homesite.mapping.model;

import java.util.List;

public class MappingRequest {

    private int uwCompany;
    private String stateCode;
    private String formCode;
    private int previousUwCompany;
    private List<Field> fields;

    public MappingRequest(int uwCompany, String stateCode, String formCode, List<Field> fields, int previousUwCompany) {
        this.uwCompany = uwCompany;
        this.stateCode = stateCode;
        this.formCode = formCode;
        this.fields = fields;
        this.previousUwCompany = previousUwCompany;
    }

    public MappingRequest() {
    }

    public int getUwCompany() {
        return uwCompany;
    }

    public void setUwCompany(int uwCompany) {
        this.uwCompany = uwCompany;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public int getPreviousUwCompany() {
        return previousUwCompany;
    }

    public void setPreviousUwCompany(int previousUwCompany) {
        this.previousUwCompany = previousUwCompany;
    }

    @Override
    public String toString() {
        return "MappingRequest{" +
                "uwCompany=" + uwCompany +
                ", stateCode='" + stateCode + '\'' +
                ", formCode='" + formCode + '\'' +
                ", previousUwCompany=" + previousUwCompany +
                ", fields=" + fields +
                '}';
    }
}
