package com.homesite.mapping.controller;

import com.homesite.mapping.model.MappingRequest;
import com.homesite.mapping.model.MappingResponse;
import com.homesite.mapping.service.MappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class MappingController {

    private MappingService mappingService;

    @Autowired
    public MappingController(MappingService mappingService) {
        this.mappingService = mappingService;
    }

    @PostMapping("/callConvertMapping")
    public MappingResponse callConvertMapping(@RequestBody @Valid MappingRequest mappingRequest) {
        return mappingService.convertMapping(mappingRequest);
    }
}
