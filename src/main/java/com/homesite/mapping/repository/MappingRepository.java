package com.homesite.mapping.repository;

import com.homesite.mapping.model.Mapping;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MappingRepository extends CrudRepository<Mapping, UUID> {
    List<Mapping> findAllByStateCodeAndUwCompanyAndPreviousUwCompany(String stateCode, int uwCompany, int previousUwCompany);
}
